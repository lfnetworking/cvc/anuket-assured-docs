# anuket-assured-docs

The Anuket Assured program - AAP (formerly OPNFV Verification Program - OVP) is an open
source, community-led compliance and verification program to demonstrate the
readiness and availability of commercial cloud native and virtualized products
and services, including NFVI, Cloud Native Infrastructure, VNFs and CNFs,
using Anuket and ONAP components. The objective is to enable a longer-term
industry effort focused on end to end system validation and interoperability
integration through all parts of the stack.

The Anuket and ONAP communities — with direct involvement from major
Communications Service Providers (CSPs), Vendors and other End Users — have now joined
forces to bring Infrastructure and Workload conformance testing. The program
is the first of its kind to combine automated compliance and verification
testing for cloud native and virtualized stacks (CNFs and VNFs), allowing
participants to establish baseline conformance and interoperability with their
commercial offerings.

The program is overseen by the LF Networking Compliance and Verification
Committee (CVC). Participation is open to all but badge recipients need to be
members of LF Networking.

## License

* [The License (Creative Commons Attribution 4.0 International)](https://creativecommons.org/licenses/by/4.0/legalcode)
