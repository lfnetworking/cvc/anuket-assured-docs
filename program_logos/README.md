# Anuket Assured Program Logos

This directory contains the Anuket Assured Program logos. These logos
may only be publicly used by participants (individuals, companies, or
products) that have submitted passing results, where those results have
also passed review and acceptance for an in-force release of AAP,
as defined within the [Program Guidelines](../program_guidelines/README.md).
