# Escalation Process

If, after submitting compliance verification results and documentation, a vendor
believes its results were evaluated unfairly or if it identifies issues with the
test plan, tools, or third party lab, it may send an appeal in writing to the
Committee.  The Committee will review the appeal and respond in writing within
30 days.

If the vendor wishes to appeal further, it may send a further appeal in writing
to the LFN Board of Directors. The Board will evaluate the appeal at its next
regular meeting.

## Exceptions and Exemptions Process

Any program participant may apply for an exception or exemption from the inclusion,
requirement, or test metrics of one or more test cases within a testing program
covered within the LFN compliance programs.  As an example, an exception or exemption
might be granted for a commercial product, where the product design or requirements
do not align with or all the test case or cases to execute or perform as expected.
The following guidelines provide a process by which a participant might be granted
such an exception or exemption.  Exceptions or exemptions shall be granted to a
specific product, for a specific test run.  Generalized exceptions or exemptions to
all products or a company at large shall not be granted.  This ensures the technical
integrity of the programs remains at their peak potential.

1. The participant shall first present a technical overview of the proposed exception
or exemption to the project(s) responsible for the development and maintenance of the
test tools used by program (i.e. exceptions or exemptions within the Anuket program
shall first be presented to one of the Reference COnformance (RC) project teams).
1. The project team(s) shall determine if the proposed exception or exemption is an
error, bug, or technical oversight within the testing, where a change to the
requirements for all testing is required or if the proposal is specific to this
instance of testing and product.
1. If project team(s) finds the proposal to be a general case, where changes to the
technical testing program are required, the project team shall follow their normal
process in the development of the corrective action, and shall release a patch to
the current, in-force program, per maintenance guidelines within this document.
1. If the project team(s) finds the proposal be specific to this instance of
testing, the project team(s) shall present the proposal to the LFN CVC committee.
The CVC committee shall be responsible to ensure the proper due diligence has been
taken and any requirement or supporting documentation is captured.
1. Once the CVC committee is satisfied, the proposed exception or exemption shall
be presented to the appropriate TSC, who shall be responsible for the final
approval / grant of the exception or exemption.
1. Any granted exception or exemption shall be documented, with appropriate
technical details, in such a way to link the exception or exemption with the
public compliance listing, thus allowing external users of the program to understand
the nature and scope of the exception or exemption.

It shall also be allowed for a preferred lab to submit a request for an exception or
exemption on behalf of a participant, without disclosing the participant’s identity
to the open community.  In such cases, the preferred lab shall work with the
community to resolve the proposed exception. If granted, the badge will
include the approved exemption details, including the participant's identity.
