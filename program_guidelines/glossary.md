<h2>Anuket Assured Glossary</h2>


| Term | Definition |
| ------ | ------ |
| Program Badge | A distinction awarded to a product as a result of compliance to a set of requirements and tests. |
| Program Mark | A digital asset issued to an organization as proof of obtaining a Program Badge. |

