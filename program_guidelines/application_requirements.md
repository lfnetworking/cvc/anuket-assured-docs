# Badge Application Requirements

## Compliance Verification Procedures Requirements

AAP compliance verification procedures leverage the tests, compliance verification tools,
and test infrastructure defined and maintained by LF Networking projects which are
included in an open source release of one of the related Series LLC. The Series LLC TSC
defines which compliance verification procedures are included as part of the AAP. Once
published, the compliance verification procedure suites will not change until the next
AAP release (except to fix bugs or errors), as described in the
[Program Management & Maintenance](program_management_and_maintenance.md).

AAP compliance verification is applicable to one or more of the following categories:

1. Hardware Platform
1. Software Platform (e.g, Virtual and Cloud Native Infrastructure – NFVI, VIM, etc.)
1. Applications (e.g. VNFs or CNFs)  
1. Orchestration (End to End management)

The scope of the criteria and requirements for each AAP release is set forth in an
addendum to this document, available as part of each Anuket Assured Program release.

## Self-Compliance Verification

Organizations may conduct compliance verification using the LF Networking-designated
tools at their facilities. The LF Networking-designated tools will ensure that the
results were produced by an unaltered version of the tools, and that the results
were unaltered.  For example, this could be accomplished by digitally verifying the
tools themselves, and signing the results file(s).  Results MUST be submitted for
review with application documentation and the logo usage agreement to LF Networking.

## Qualified Verification Labs

Vendors may request service from third parties to conduct LF Networking compliance verification and collect results instead of performing the testing in-house.The compliance verification results and documentation may be submitted by the third party on behalf of the vendor requestingthe use of LF Networking Program Marks.

The Compliance and Verification Committee is responsible for setting of requirements and qualifications for the labs, and for the approval of Qualified Labs. The Linux Foundation Anuket Assured Program will maintain the list of Qualified Labs on its website, and the list will clearly indicate the testing the labs are qualified to provide services for within the program.

In general, participants of the various testing programs may use a laboratory at their convenience.  Qualified Labs are not specifically endorsed or sponsored by Linux Foundation Networking, beyond meeting the qualifications outlined in Anuket Assured Labs[link]. It is expected participants using a lab will enter a commercial agreement with the lab, and the terms of such an agreement are outside the scope of the Qualified Labs Program requirements and documentation. The laboratory may submit results to the formal review processes on behalf of its customer and the review processes will support this workflow.

## Compliance Application Requirements

The use of the program logo is not restricted to LF Networking member companies.
The request for use of LF Networking program marks must state the organization, a
contact person (email and telephone number); their postal address; the location
where the verification results and documentation was prepared, the category or
categories the product is requesting a program marks(s) for; the attestation
stating they will abide by the policies and procedures for use of the program
marks; any third-party lab conducting the verification; and the product identifier.

## Badge application process 

![AAP badge application process](../program_logos/badge-application-process.png)

Follow the instructions to earn Anuket Assured Badge:

* Step 1:  Start by reviewing the Anuket Assured Terms and Conditions (https://lfnetworking.org/wp-content/uploads/sites/7/2022/05/Anuket_Assured_Terms_and_Conditions_010322.pdf)

* Step 2: Complete and submit the Participation Form (https://powerforms.docusign.net/b8cf1622-3566-4613-af62-8b913ecc9c52?env=na3&acct=f30e10ec-fea1-4dd8-a262-384a61edabb5&accountId=f30e10ec-fea1-4dd8-a262-384a61edabb5) 

* Step 3: Conduct the appropriate tests based for the badge to be earned (See the section titled "Scope of Testing for This Release" in the Program Release Documentation (https://gitlab.com/lfnetworking/cvc/anuket-assured-docs/-/tree/main/releases/2022.10)

* Step 4:  Submit the results for CVC review by mailing to anuketassured@lfnetworking.org 

    The compliance verification results and documentation submitted will be reviewed by the Committee for completeness and validity.Based on the determination of the Committee, a recommendation will be made to the LFN Board of Directors regarding
the approval of the granting of permission to use the program marks.

    The Committee may request additional information regarding the application for use of the program marks.

* Step 5:  Upon notification of verified and successful compliance, the appropriate Anuket Assured badge(s) will be awarded.


Please also pay attention to the following items in the badge application process：

The LFN may charge a reasonable fee for reviewing results.  Reviews will be
conducted by LFN member companies participating in the review committee
(CVC subcommittee). No member company may review its own compliance verification
results.

In the event of a dispute, the submitting organization has a right to appeal the
decision with the LFN Board of Directors. An appeals process is documented in the
[Escalation Process](escalation_process.md).
