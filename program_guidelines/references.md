# Program References

The following references and points of are available for the Anuket Assured
Program.

* Anuket Assured Program Website: <https://www.lfnetworking.org/ovp/>
* Compliance & Verification Committee Email List: <lfn-compliance@lists.lfnetworking.org>
