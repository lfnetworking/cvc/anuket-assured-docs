# Executive Summary

LF Networking is offering compliance verification through its Anuket Assured
Program (AAP). The AAP verifies products (“Offerings”) that claim compliance to
Anuket, ONAP, and/or other LFN projects.

The LFN AAP is a compliance verification program intended to increase the awareness
and adoption of LF Networking technology by demonstrating the readiness and
availability of commercial products using LFN Projects.

The scope of the AAP includes verification of NFV based infrastructure, Cloud Native Infrastructure, 
VNF and CNF workloads, using Anuket and ONAP network management and orchestration components. 

The goal is to enable an industry effort focused on end-to-end system validation and interoperability 
integration through all parts of the stack.

The key objectives and benefits of the LFN AAP are to:

* Help build the market for LFN-based SDN/NFV/automation infrastructure and
applications or other devices designed to interface with that infrastructure
* Promote interoperability between "apps", such as CNFs or VNFs and their
virtual and cloud native infrastructure
* Reduce adoption risks for end-users
* Decrease testing costs by verifying hardware and software platform
interfaces and components

Specifically, the benefits visible to service providers and vendors are as follows:

For Service Providers:

* Accelerate time to deployment for new network services.
* Improve interoperability and software quality.
* Reduce in-house testing effort and reduce costs.
* Improved utilization of hardware

For Vendors:
* Improve time to revenue for new product offerings.
* Achieve greater alignment with service provider customer requirements.
* Demonstrate product quality through open ecosystem testing. 
* Leveraging the community to reduce in-house effort

Verified offerings submitted by respective vendors are expected to differentiate
themselves with different features and capabilities but remain compliant by
implementing explicitly defined interfaces, behaviors, and key features.
